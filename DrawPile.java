public class DrawPile extends DynamicCardArray{
    private DynamicCardArray drawPile = new DynamicCardArray();
    private int called;

    public DrawPile(DynamicCardArray deck){
        this.drawPile = deck;
        for (Card card : this.drawPile.getData()){
            if(card == null){
                continue;
            }
            card.show();
        }
        this.called = 0;
    }

    public DynamicCardArray getDrawPile(){
        return this.drawPile;
    }

    public void removeFromPile(Card removeMe){
        if(this.drawPile.length() == 0){
            throw new IllegalArgumentException("The draw pile is empty");
        }
        if(this.drawPile.contains(removeMe)){
            int i = this.called;
            if(this.drawPile.get(i).equals(removeMe)){
                this.drawPile.removeElement(i);;
            }
            else{
                throw new IllegalArgumentException("Invalid input, can only move a visible card");
            }
        }
        //cycles back to the first card in the pile when the final card is removed
        this.called++;
        if(this.called == this.drawPile.length()){
            called = 0;
        }
        
    }

    public boolean validCardMove(Card cardToCheck){
        if(this.drawPile.contains(cardToCheck)){
            int i = this.called;
            if(this.drawPile.get(i).equals(cardToCheck)){
                return true;
            }
            else{
                throw new IllegalArgumentException("Can only move the displayed card");
            }
        }
        else{
            return false;
        }
    }

    public void nextCard(){
        //allows the player to skip to the next card in the drawpile
        this.called++;
        if(this.called == this.drawPile.length()){
            called = 0;
        }
    }

    public boolean contains(Card check){
        for(Card x : this.drawPile.getData()){
            if(x == null){
                continue;
            }
            if(x.equals(check)){
                return true;
            }
        }
        return false;
    }
    public void update(){
        DynamicCardArray temp = this.drawPile;
        this.drawPile = temp;
    }
    
    @Override
    public String toString(){
        String out = "";
            out += String.format("There are %s cards in the draw pile \n", this.drawPile.length());
            out += this.drawPile.get(this.called);
            out+="\n";
        return out;
    }
}
