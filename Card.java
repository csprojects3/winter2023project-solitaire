
public class Card {
    private Suit suit;
    private Rank rank;

    private String card;
    private boolean visible;

    public Card(Suit suit, Rank rank){
        this.suit = suit;
        this.rank = rank;
        this.card = this.rank.toString() + this.suit.toString();
        this.visible = false;
    }
    
    public Card(){
        //blank card
        this.card = "";
        this.rank = Rank.DEFAULT;
        this.suit = Suit.DEFAULT;
        this.visible = true;
    }
    
    public void hide(){ //this method is used to show the back of a card
        if(this.visible){
            this.show();
            return;
        }
        this.visible=false;
    }

    public void show(){ //this method is used to show the value of the card
        this.visible = true;
    }

    public boolean visible(){
        if(this.visible){
            return true;
        }
        return false;
    }

    public Suit getSuit(){
        if(this.suit == null){
            return Suit.DEFAULT;
        }
        return this.suit;
    }

    public Rank getRank(){
        if(this.rank == null){
            return Rank.DEFAULT;
        }
        return this.rank;
    }
    public int getRankValue(){
        return rankValue(this.rank);
    }
    //get a random card
    public static Card ranCard(){
        Card ranCard = new Card(Suit.randomSuit(),Rank.randomRank());
        return ranCard;

    }
    public boolean hidden(){
        if(this.visible){
            return false;
        }
        return true;
    }

    public boolean placeUnder(Card top){
        //if the top card is null ie the column is 'empty' return true
        if(top.getRank().equals(Rank.DEFAULT) || top == null || top.getRank() == null){
            if(!this.rank.equals(Rank.KING)){
                throw new IllegalArgumentException("Rows can only start with a King card");
            }
            return true;
        }

        //values for card that goes ontop
        Rank topRank = top.getRank();
        Suit topSuit = top.getSuit();
        //values for the card that goes underneath
        Rank bottomRank = this.getRank();
        Suit bottomSuit = this.getSuit();

        String topColour = "";
        String bottomColour = "";
        if(topSuit.equals(Suit.DIAMOND) || topSuit.equals(Suit.HEART)){
            topColour = "red";
        }
        else{
            topColour = "black";
        }
        if(bottomSuit.equals(Suit.DIAMOND) || bottomSuit.equals(Suit.HEART)){
            bottomColour = "red";
        }
        else{
            bottomColour = "black";
        }
        if(topColour.equals(bottomColour)){
            throw new IllegalArgumentException("Cannot put " + bottomSuit + " on " + topSuit);
        }
        if(rankValue(topRank) != rankValue(bottomRank) + 1){
            throw new IllegalArgumentException("Can only place cards on a card that has a rank one above it");
        }
        return true;
    }
    private int rankValue(Rank cardRank){
        int value = -1;
        switch(cardRank){
            case ACE:
                value = 1;
                break;
            case TWO :
                value = 2;
                break;
            case THREE :
                value = 3;
                break;
            case FOUR :
                value = 4;
                break;
            case FIVE :
                value = 5;
                break;
            case SIX :
                value = 6;
                break;
            case SEVEN :
                value = 7;
                break;
            case EIGHT :
                value = 8;
                break;
            case NINE :
                value = 9;
                break;
            case TEN :
                value = 10;
                break;
            case JACK :
                value = 11;
                break;
            case QUEEN :
                value = 12;
                break;
            case KING :
                value = 13;
                break;
            case DEFAULT :
                value = 14;
                break;
        }
        return value;
    }
    @Override
    public boolean equals (Object obj){
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        final Card compare = (Card) obj;
        if(!(this.getRank().equals(compare.getRank())) || !(this.getSuit().equals(compare.getSuit()))){
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        String out = "";
            if(this.visible){
                if(this.getSuit().equals(Suit.SPADE) || this.getSuit().equals(Suit.CLUB)){
                    out += ( Colours.BLACK.toString() + "[" + this.card + "]" + Colours.RESET);
                }
                else if(this.getSuit().equals(Suit.HEART) || this.getSuit().equals(Suit.DIAMOND)){
                    out += ( Colours.RED.toString() + "[" + this.card + "]" + Colours.RESET);
                }
                else{
                    out += this.card;
                }
               
            }   
            else if(!visible){
                out+="XX";
            }
            else{
                out += "";
            }   
        return out;
    
    }
}
