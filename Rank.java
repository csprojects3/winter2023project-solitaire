import java.util.Random;
public enum Rank {
    ACE("A"),TWO("2"),THREE("3"),FOUR("4"),FIVE("5"),SIX("6"),SEVEN("7"),
    EIGHT("8"),NINE("9"),TEN("10"),JACK("J"),QUEEN("Q"),KING("K"),DEFAULT("");

    private String rank;

    private Rank(String rank){
        this.rank = rank;
    }
    
    public static Rank randomRank(){
        
        Random ran = new Random();
        int x;
        Rank ranRank = DEFAULT;
        x = ran.nextInt(13)+1;
        
        switch(x){
            case 1:
            ranRank = ACE;
            break;
            case 2:
            ranRank = TWO;
            break;
            case 3:
            ranRank = THREE;
                break;
            case 4:
            ranRank = FOUR;
            break;
            case 5:
            ranRank = FIVE;
            break;
            case 6:
            ranRank = SIX;
            break;
            case 7:
            ranRank = SEVEN;
            break;
            case 8:
            ranRank = EIGHT;
            break;
            case 9:
            ranRank = NINE;
            break;
            case 10:
            ranRank = TEN;
            break;
            case 11:
            ranRank = JACK;
            break;
            case 12:
            ranRank = QUEEN;
            break;
            case 13:
            ranRank = KING;
            break;
        }
        return ranRank;
    }

    public boolean equals (Rank obj){
        if(obj == null){
            return false;
        }
        if(this.getClass() != obj.getClass()){
            return false;
        }
        if(this != obj){
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        return this.rank;
    }
}
