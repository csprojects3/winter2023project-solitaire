import java.util.Random;
public class Deck {
    private DynamicCardArray deck = new DynamicCardArray();
    private Random ran = new Random();
    public Deck(){
        Rank[] ranks = {Rank.ACE, Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE, Rank.SIX, Rank.SEVEN,
                        Rank.EIGHT, Rank.NINE, Rank.TEN, Rank.QUEEN, Rank.JACK, Rank.KING};
        Suit[] suits = {Suit.HEART, Suit.DIAMOND,Suit.SPADE,Suit.CLUB};
        Card toAdd = new Card();
        int i = 0;
        while( i < 52){
            for(Rank rank : ranks){
                for(Suit suit : suits){
                    toAdd = new Card(suit, rank);
                    this.deck.add(toAdd);
                    i++;
                }
            }
        }
        this.shuffle();
    }

    public DynamicCardArray getDeck(){
        return this.deck;
    }

    private void shuffle(){
        int j = -1;
        DynamicCardArray tempDeck = new DynamicCardArray();
        for(int i = 0; i < deck.length(); i++){
            j = ran.nextInt(52);
            if(tempDeck.contains(this.deck.get(j))){
                i--;
                continue;
            }
            tempDeck.add(this.deck.get(j));
        }
        this.deck = tempDeck;
    }
    
    public void removeCard(Card removeMe)
    {
        int index = this.deck.getIndexOf(removeMe);
        this.deck.removeElement(index);
    }   

    public boolean contains(Card check){
        return this.deck.contains(check);
    }

    //testing method
    public int IndexOf(Card search){
        return this.deck.getIndexOf(search);
    }
    //testing method
    public int length(){
        return this.deck.length();
    }
    //testing method
    public void addCard(Card toAdd){
        this.deck.add(toAdd);
    }

    @Override
    public String toString(){
        String out = "";
        for (int i = 0; i<this.deck.length(); i++){
                if(this.deck.get(i)==null){
                    continue;
                }
                out += this.deck.get(i) + " ";
                out +=", ";
        }
        return out;
    }
}
